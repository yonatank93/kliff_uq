#!/usr/bin/env python
# coding: utf-8

# In this example, we demonstrate how to perform uncertainty quantification (UQ) using
# MCMC method. We use a Stillinger-Weber (SW) potential for silicon that is archived in
# [OpenKIM](https://openkim.org/).
#
# Before getting started, make sure that the SW model is installed.


# !kim-api-collections-management install user SW_StillingerWeber_1985_Si__MO_405512056662_006


# For simplicity, we only set the energy-scaling parameters, i.e., $A$ and $\lambda$ as
# the tunable parameters. Furthermore, these parameters are physically constrained to be
# positive, thus we will work in log parameterization, i.e. $\log(A)$ and $\log(\lambda)$.
# These parameters will be calibrated to energies and forces of a small dataset,
# consisting of 4 compressed and stretched configurations of diamond silicon structure.

import numpy as np
from multiprocessing import Pool

from kliff.calculators import Calculator
from kliff.dataset import Dataset
from kliff.dataset.weight import MagnitudeInverseWeight
from kliff.loss import Loss
from kliff.models import KIMModel
from kliff.models.parameter_transform import LogParameterTransform
from kliff.utils import download_dataset

from kliff.uq import MCMC, get_T0, mser, autocorr, rhat


#########################################################################################
# First, we instantiate the parameter transformation class and create a KIM model for the
# SW potential.


# Instantiate a transformation class to do the log parameter transform
param_names = ["A", "lambda"]
params_transform = LogParameterTransform(param_names)


# Create the model
model = KIMModel(
    model_name="SW_StillingerWeber_1985_Si__MO_405512056662_006",
    params_transform=params_transform,
)
# model.echo_model_params(params_space="original")
model.echo_model_params(params_space="transformed")


# Then, we specify the tunable parameters and the initial guess. Furthermore, we can also
# specify the boundaries of the allowed value. In the UQ formulation, the bounds
# information will be used to define the boundaries of the uniform prior.


# Set the tunable parameters and the initial guess
opt_params = {
    "A": [["default"]],
    "lambda": [["default"]],
}
model.set_opt_params(**opt_params)
model.echo_opt_params()


#########################################################################################
# The next step is to set the training set. In this step, we also need to specify how we
# want to compute the weight, by instantiating the class that is used to compute and
# store weight information. We then create a calculator based on the configurations read
# from the dataset.


# Get the dataset and set the weights
dataset_path = download_dataset(dataset_name="Si_training_set_4_configs")
# Instantiate the weight class
weight = MagnitudeInverseWeight(
    weight_params={
        "energy_weight_params": [0.0, 0.1],
        "forces_weight_params": [0.0, 0.1],
    }
)
# Read the dataset and compute the weight
tset = Dataset(dataset_path, weight=weight)
configs = tset.get_configs()


# Create calculator
calc = Calculator(model)
ca = calc.create(configs)


#########################################################################################
# We also need to define the loss function and train the potential to obtain the optimal
# parameters. These parameters will be used to estimate the model bias.


# Instantiate the loss function
residual_data = {"normalize_by_natoms": False}
loss = Loss(calc, residual_data=residual_data)


# Train the model
loss.minimize(method="lm")
model.echo_opt_params()


#########################################################################################
# Finally, we are ready to perform MCMC sampling. For this example, we show how KLIFF
# would interface with ``emcee`` Python package. This package utilizes affine invariance
# property of MCMC sampling.


# It is a good practice to specify the random seed to generate a reproducible simulation.
seed = 1717
np.random.seed(seed)

# Get the dimensionality of the problem
ndim = calc.get_num_opt_params()  # Number of parameters
nwalkers = 2 * ndim  # Number of parallel walkers to simulate


# The wrapper in KLIFF generates the likelihood from the loss function and temper the
# loss function with a natural temperature to model the bias. Additionally, we can
# specify the prior (or log-prior to be more precise), with the default be a uniform
# prior that is bounded over a finite range that we specify, or otherwise it will retrieve
# the bounds from the model (see ``kliff.models.KIMModel.set_opt_params()``).


# Create sampler
prior_bounds = np.tile([-8, 8], (ndim, 1))
T0 = get_T0(loss)  # Sampling temperature
sampler = MCMC(loss, T=T0, logprior_args=(prior_bounds,), use_ptsampler=False)

# Initial starting point. This should be provided by the user.
p0 = np.empty((nwalkers, ndim))
for ii, bound in enumerate(prior_bounds):
    p0[:, ii] = np.random.uniform(*bound, nwalkers)

# Run MCMC
sampler.sampler.pool = Pool(nwalkers)  # Declare multiprocessing pool
sampler.run_mcmc(p0, 10000, progress=True)
sampler.sampler.pool.close()

# Retrieve the chain
chain = sampler.chain


#########################################################################################
# The resulting chains still need to processed. First, we need to discard the first few
# iterations in the beginning of each chain as a burn-in time. This is similar to the
# Equilibration time in a molecular dynamic simulation before we can start the
# measurement. Then, in general the i-th step in MCMC is not independent to the (i+1)-th
# step. However after several iteration, the walkers will forget their initial positions.
# So that the (i+:math:`\tau`)-th step is independent to the i-th step. :math:`\tau` is
# known as the autocorrelation length. Finally, after obtaining the independent samples,
# i.e. by discarding the burn-in time and thin the chains by the autocorrelation length,
# we need to assess whether the resulting samples have converged to a stationary
# distribution, and thus a good representation of the actual posterior. This is done by
# computing the potential scale reduction factor (PSRF). The PSRF declines to 1 as the
# number of iterations goes to infinity. A common threshold is about 1.1, but higher
# threshold has also been used. In this example,we set the threshold to higher value of
# 1.6, since there might be a numerical error due to tight posterior distribution of the
# samples.


# Estimate equilibration time using MSER for each walker, and dimension.
mser_array = np.empty((nwalkers, ndim))
for widx in range(nwalkers):
    for pidx in range(ndim):
        mser_array[widx, pidx] = mser(chain[:, widx, pidx], dmin=0, dstep=10, dmax=-1)

burnin = int(np.max(mser_array))
print(f"Estimated burn-in time: {burnin}")

# Estimate the autocorrelation length
acorr_array = autocorr(chain[:, burnin:], c=1, quiet=True)
thin = int(np.ceil(np.max(acorr_array)))
print(f"Estimated autocorrelation length: {thin}")

# Assess the convergence
samples = chain[:, burnin::thin]

threshold = 1.1  # Threshold for rhat
rhat_val = rhat(samples)

print(f"$\hat{{r}}^p$ value: {rhat_val}")
