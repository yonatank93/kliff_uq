#!/usr/bin/env python
# coding: utf-8

# This script was used to generate the results presented in our eScience paper. We
# perform uncertainty quantification (UQ) using parallel tempered MCMC (PTMCMC). We use a
# Stillinger-Weber (SW) potential for silicon that is archived in
# [OpenKIM](https://openkim.org/).
#
# Before getting started, make sure that the SW model is installed.


# !kim-api-collections-management install user SW_StillingerWeber_1985_Si__MO_405512056662_006


# For simplicity, we only set the energy-scaling parameters, i.e., $A$ and $\lambda$ as
# the tunable parameters. Furthermore, these parameters are physically constrained to be
# positive, thus we will work in log parameterization, i.e. $\log(A)$ and $\log(\lambda)$.
# These parameters will be calibrated to energies and forces of a small dataset,
# consisting of 4 compressed and stretched configurations of diamond silicon structure.

import os

import numpy as np
from multiprocessing import Pool

from kliff.calculators import Calculator
from kliff.dataset import Dataset
from kliff.dataset.weight import MagnitudeInverseWeight
from kliff.loss import Loss
from kliff.models import KIMModel
from kliff.models.parameter_transform import LogParameterTransform
from kliff.utils import download_dataset

from kliff.uq import MCMC, get_T0
from kliff.uq import mser, autocorr, rhat


#########################################################################################
# First, we instantiate the parameter transformation class and create a KIM model for the
# SW potential.


# Instantiate a transformation class to do the log parameter transform
param_names = ["A", "B", "sigma", "lambda", "gamma"]
params_transform = LogParameterTransform(param_names)


# Instantiate the model and set the potential
model = KIMModel(
    model_name="SW_StillingerWeber_1985_Si__MO_405512056662_006",
    params_transform=params_transform,
)
# model.echo_model_params(params_space="original")
model.echo_model_params(params_space="transformed")


# Then, we specify the tunable parameters and the initial guess. Furthermore, we can also
# specify the boundaries of the allowed value. In the UQ formulation, the bounds
# information will be used to define the boundaries of the uniform prior.


# Set the tunable parameters and the initial guess
opt_params = {name: [["default"]] for name in param_names}
model.set_opt_params(**opt_params)
model.echo_opt_params()


#########################################################################################
# The next step is to set the training set. In this step, we also need to specify how we
# want to compute the weight, by instantiating the class that is used to compute and
# store weight information. We then create a calculator based on the configurations read
# from the dataset.


# Get the dataset and set the weights
dataset_path = download_dataset(dataset_name="Si_training_set")
# To speed up the calculation, we actually will only use part of the configurations
dataset_path /= "varying_alat"
# Instantiate the weight class
weight = MagnitudeInverseWeight(
    # Each key in weight_params contains a list [c_1, c_2]
    weight_params={
        "energy_weight_params": [0.0, 0.1],
        "forces_weight_params": [1e-2, 0.1],
    }
)
# Read the configurations and reference data from the dataset
tset = Dataset(dataset_path, weight=weight)
configs = tset.get_configs()


# Create a calculator, which consists of simulationso to compute material properties
calc = Calculator(model)
# Set the configurations to use by the calculator
ca = calc.create(configs)


#########################################################################################
# We also need to define the loss function and train the potential to obtain the optimal
# parameters. These parameters will be used to estimate the model bias.


# Instantiate the loss function
residual_data = {"normalize_by_natoms": False}
loss = Loss(calc, residual_data=residual_data)

# Train the model
loss.minimize(method="lm")
model.echo_opt_params()

#########################################################################################
# Finally, we are ready to perform MCMC sampling. For this example, we show  how KLIFF
# would interface with ``ptemcee`` Python package. This package perform parallel tempered
# MCMC and utilizes the affine invariance property of MCMC sampling. We simulate MCMC
# sampling at several different temperatures to explore the effect of the scale of bias
# and overall error bars.


# It is a good practice to specify the random seed to generate a reproducible simulation.
seed = 2022
np.random.seed(seed)

# Get the dimensionality of the problem
ndim = calc.get_num_opt_params()  # Number of parameters
nwalkers = 2 * ndim  # Number of parallel walkers to simulate

# Generate a temperature ladder
T0 = get_T0(loss)
Tladder = np.sort(np.append(np.logspace(0, 7, 15), T0))
ntemps = len(Tladder)  # Number of temperatures to simulate


# The wrapper in KLIFF generates the likelihood from the loss function. Additionally, we
# can specify the prior (or log-prior to be more precise), with the default be a uniform
# prior that is bounded over a finite range that we specify, or otherwise it will retrieve
# the bounds from the model (see ``kliff.models.KIMModel.set_opt_params()``).
#
# To specify the temperatures to use, we can specify how many temperature to simulate
# (``ntemps``) and the ratio of the highest temperature to the natural temperature
# :math:`T_0` (``Tmax_ratio``). It has been shown that including temperatures higher than
# :math:`T_0` helps the convergence of walkers sampled at :math:`T_0`. Alternatively, we
# can also give a list of the temperatures to use.
#
# Then for this script, parallelization is done for the sampling process using the
# ``multiprocessing`` Python package. This is done by declaring the pool after
# instantiating the sampler, but before running sampling.

chain_file = "data/ptemcee_SW_chain.npy"
if not os.path.isfile(chain_file):
    # Create sampler
    sampler = MCMC(
        loss,
        nwalkers=nwalkers,
        logprior_args=(np.tile([-8, 8], (ndim, 1)),),
        Tladder=Tladder,
        # Other keyword arguments for ptemcee.Sampler
        random=np.random.RandomState(seed),
    )

    # Initial starting point. This should be provided by the user.
    p0 = np.random.uniform(low=-6.0, high=6.0, size=(ntemps, nwalkers, ndim))

    # Run MCMC
    sampler.sampler.pool = Pool(processes=nwalkers)
    sampler.run_mcmc(p0, 150000)
    sampler.sampler.pool.close()

    # Retrieve the chain
    chain = sampler.chain

    # Export the chains
    np.save(chain_file, chain)

else:
    # Load the chains
    chain = np.load(chain_file)


#########################################################################################
# The resulting chains still need to processed. First, we need to discard the first few
# iterations in the beginning of each chain as a burn-in time. This is similar to the
# Equilibration time in a molecular dynamic simulation before we can start the
# measurement. Then, in general the i-th step in MCMC is not independent to the (i+1)-th
# step. However after several iteration, the walkers will forget their initial positions.
# So that the (i+:math:`\tau`)-th step is independent to the i-th step. :math:`\tau` is
# known as the autocorrelation length. Finally, after obtaining the independent samples,
# i.e. by discarding the burn-in time and thin the chains by the autocorrelation length,
# we need to assess whether the resulting samples have converged to a stationary
# distribution, and thus a good representation of the actual posterior. This is done by
# computing the potential scale reduction factor (PSRF). The PSRF declines to 1 as the
# number of iterations goes to infinity. A common threshold is about 1.1, but higher
# threshold has also been used.


# Estimate equilibration time using MSER for each temperature, walker, and dimension.
burnin_file = "data/ptemcee_SW_burnin.npy"
if not os.path.isfile(burnin_file):
    burnin_array = np.empty((ntemps, nwalkers, ndim))
    for tidx in range(ntemps):
        for widx in range(nwalkers):
            for pidx in range(ndim):
                burnin_array[tidx, widx, pidx] = mser(
                    chain[tidx, widx, :, pidx], window=1, dmin=0, dstep=10, dmax=-1
                )
    np.save(burnin_file, burnin_array)
else:
    burnin_array = np.load(burnin_file)

# To be safe, we will set the burn-in time to be larger than the estimated value.
est_burnin = int(np.max(burnin_array))
burnin = 10000
print(f"Estimated burn-in time: {est_burnin}, set to: {burnin}")


# Estimate the autocorrelation length for each temperature
acorr_file = "data/ptemcee_SW_acorr.npy"
if not os.path.isfile(acorr_file):
    acorr_array = np.empty((ntemps, nwalkers, ndim))
    for tidx in range(ntemps):
        acorr_array[tidx] = autocorr(np.swapaxes(chain[tidx], 0, 1), c=1, quiet=True)
    np.save(acorr_file, acorr_array)
else:
    acorr_array = np.load(acorr_file)

# Similarly, we will set the thinning factor to be larger than the estimated
# autocorrelation length.
est_thin = int(np.ceil(np.max(acorr_array)))
thin = 200
print(f"Estimated autocorrelation length: {est_thin}, set to: {thin}")


# Assess the convergence for each temperature
samples = chain[:, :, burnin::thin]

rhat_file = "data/ptemcee_SW_rhat.npy"
if not os.path.isfile(rhat_file):
    rhat_array = np.empty(ntemps)
    for tidx in range(ntemps):
        rhat_array[tidx] = rhat(samples[tidx])
    np.save(rhat_file, rhat_array)
else:
    rhat_array = np.load(rhat_file)

print("$\hat{{r}}^p$ values:\n", rhat_array)
